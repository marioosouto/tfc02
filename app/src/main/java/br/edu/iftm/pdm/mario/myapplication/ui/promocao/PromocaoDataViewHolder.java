package br.edu.iftm.pdm.mario.myapplication.ui.promocao;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.edu.iftm.pdm.mario.myapplication.R;
import br.edu.iftm.pdm.mario.myapplication.model.Promocao;
import br.edu.iftm.pdm.mario.myapplication.ui.promocao.PromocaoDataAdapter;


public class PromocaoDataViewHolder extends RecyclerView.ViewHolder
                                    implements View.OnClickListener {

    private final TextView txtPromocao;
    private final TextView txtDescricaoPromo;
    private PromocaoDataAdapter adapter;
    private Promocao currentPromocao;
    private ImageView imgEye;



    public PromocaoDataViewHolder(@NonNull View itemView, PromocaoDataAdapter adapter) {
        super(itemView);
        this.txtPromocao = itemView.findViewById(R.id.txtPromocao);
        this.txtDescricaoPromo = itemView.findViewById(R.id.txtDescricaoPromo);
        this.adapter = adapter;
        itemView.setOnClickListener(this);
        this.imgEye.setOnClickListener(this);
    }

    public void bind(Promocao promocao){
        this.txtPromocao.setText(promocao.getNome());
        this.txtDescricaoPromo.setText(String.valueOf(promocao.getDescricao()));
        this.currentPromocao = promocao;
    }


    @Override
    public void onClick(View v) {
        if (this.adapter.getOnClickPromocaoListener() != null){
            if(v == this.imgEye) {
                this.adapter.getOnClickPromocaoListener().onclickPromocao(this.currentPromocao);
            }
        }
    }
}
