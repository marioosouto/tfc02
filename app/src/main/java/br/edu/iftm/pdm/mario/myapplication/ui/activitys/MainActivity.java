package br.edu.iftm.pdm.mario.myapplication.ui.activitys;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import br.edu.iftm.pdm.mario.myapplication.R;
import br.edu.iftm.pdm.mario.myapplication.data.DAOPizzasSingleton;
import br.edu.iftm.pdm.mario.myapplication.model.Pizza;

public class MainActivity extends AppCompatActivity {

    private static final int FORM_REQUEST_CODE = 1234;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void onClickNovaPizza(View view) {
        Intent openNovaPizza = new Intent(this, NewPizzaActivity.class);
        startActivityForResult(openNovaPizza, FORM_REQUEST_CODE);
    }

    public void onClickListaPizza(View view) {
        Intent listaPizzas = new Intent(this, PizzaMenuActivity.class);
        startActivity(listaPizzas);
    }


    public void onClickListaPromocoes(View view) {
      Intent listaPromocoes = new Intent(this, PromocoesActivity.class);
       startActivity(listaPromocoes);
   }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FORM_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            Pizza pizza = data.getParcelableExtra(NewPizzaActivity.RESULT_KEY);
            DAOPizzasSingleton.getINSTANCE().addPizza(pizza);
        }
    }
}