package br.edu.iftm.pdm.mario.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Pizza implements Parcelable {

    private String sabor;
    private String descricao;
    private float valor;

    public Pizza(String sabor, String descricao, float valor) {
        this.sabor = sabor;
        this.descricao = descricao;
        this.valor = valor;
    }

    public String getSabor() {
        return sabor;
    }

    public String getDescricao() {
        return descricao;
    }

    public float getValor() {
        return valor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sabor);
        dest.writeString(descricao);
        dest.writeFloat(valor);
    }

    protected Pizza(Parcel in) {
        sabor = in.readString();
        descricao = in.readString();
        valor = in.readFloat();
    }

    public static final Creator<Pizza> CREATOR = new Creator<Pizza>() {
        @Override
        public Pizza createFromParcel(Parcel in) {
            return new Pizza(in);
        }

        @Override
        public Pizza[] newArray(int size) {
            return new Pizza[size];
        }
    };

}
