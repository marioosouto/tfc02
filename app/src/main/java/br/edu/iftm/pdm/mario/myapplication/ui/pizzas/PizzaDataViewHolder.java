package br.edu.iftm.pdm.mario.myapplication.ui.pizzas;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import br.edu.iftm.pdm.mario.myapplication.R;
import br.edu.iftm.pdm.mario.myapplication.model.Pizza;


public class PizzaDataViewHolder extends RecyclerView.ViewHolder
                                    implements View.OnClickListener {

    private final TextView txtSaborLabel;
    private final TextView txtValorLabel;
    private final TextView txtDescricaoLabel;
    private PizzaDataAdapter adapter;
    private Pizza currentPizza;


    public PizzaDataViewHolder(@NonNull View itemView, PizzaDataAdapter adapter) {
        super(itemView);
        this.txtSaborLabel = itemView.findViewById(R.id.txtSabor);
        this.txtValorLabel = itemView.findViewById(R.id.txtValorLabel);
        this.txtDescricaoLabel = itemView.findViewById(R.id.txtDescricao);
        this.adapter = adapter;
        itemView.setOnClickListener(this);
        this.txtValorLabel.setOnClickListener(this);
    }

    public void bind(Pizza pizza){
        this.txtSaborLabel.setText(pizza.getSabor());
        this.txtValorLabel.setText(String.valueOf(pizza.getValor()));
        this.txtDescricaoLabel.setText(pizza.getDescricao());
        this.currentPizza = pizza;
    }


    @Override
    public void onClick(View v) {
        if (this.adapter.getOnClickPizzaListener() != null){
            if(v == this.txtValorLabel) {
                this.adapter.getOnClickPizzaListener().onclickPizza(this.currentPizza);
            }
        }
    }
}
