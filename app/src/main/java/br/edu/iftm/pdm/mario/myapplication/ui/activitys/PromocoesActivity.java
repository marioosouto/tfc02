package br.edu.iftm.pdm.mario.myapplication.ui.activitys;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import br.edu.iftm.pdm.mario.myapplication.R;
import br.edu.iftm.pdm.mario.myapplication.data.DAOPromocoesSingleton;
import br.edu.iftm.pdm.mario.myapplication.model.Promocao;
import br.edu.iftm.pdm.mario.myapplication.ui.promocao.PromocaoDataAdapter;

public class PromocoesActivity extends AppCompatActivity {

    private RecyclerView rvPromocoes;
    private PromocaoDataAdapter promocaoDataAdapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promocoes);

        this.rvPromocoes = findViewById(R.id.rvPromocoes);

        this.layoutManager = new LinearLayoutManager(this);
        this.promocaoDataAdapter = new PromocaoDataAdapter(DAOPromocoesSingleton.getINSTANCE().getPromocoes());
        this.promocaoDataAdapter.setOnClickPromocaoListener(new PromocaoDataAdapter.OnClickPromocaoListener() {
            @Override
            public void onclickPromocao(Promocao promocao) {
                showPromoInfo(promocao);
            }
        });

        DividerItemDecoration divider = new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);

        this.rvPromocoes.setLayoutManager(this.layoutManager);
        this.rvPromocoes.setHasFixedSize(true);
        this.rvPromocoes.addItemDecoration(divider);
        this.rvPromocoes.setAdapter(this.promocaoDataAdapter);
    }

    private void showPromoInfo(Promocao promocao) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.promo_info);
        View view = getLayoutInflater().inflate(R.layout.show_promo_info_dialog, null);
        dialogBuilder.setView(view);
        TextView txtSaborPizza = view.findViewById(R.id.txtDescPromocao);
        txtSaborPizza.setText(promocao.getNome());
        TextView txtDescricaoPizza = view.findViewById(R.id.txtDescontoPromocao);
        txtDescricaoPizza.setText(promocao.getDescricao());
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.create().show();
    }
}
