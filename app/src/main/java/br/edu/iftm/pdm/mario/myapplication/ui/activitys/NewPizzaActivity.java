package br.edu.iftm.pdm.mario.myapplication.ui.activitys;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import br.edu.iftm.pdm.mario.myapplication.R;
import br.edu.iftm.pdm.mario.myapplication.model.Pizza;

public class NewPizzaActivity extends AppCompatActivity {


    public static final String RESULT_KEY = "NewPizzaActivity.RESULT_KEY";
    private TextView etxtPizzaSabor;
    private TextView etxtIngredientes;
    private TextView etxtPrecoPizza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pizza);
        this.etxtPizzaSabor = findViewById(R.id.etxtNomePizza);
        this.etxtIngredientes = findViewById(R.id.etxtIngredientes);
        this.etxtPrecoPizza = findViewById(R.id.etxtPrecoPizza);
    }


    public void onClickSalvar(View view) {

        String sabor = this.etxtPizzaSabor.getText().toString();
        String ingredientes = this.etxtIngredientes.getText().toString();
        String preco = this.etxtPrecoPizza.getText().toString();

        if (sabor.isEmpty() || ingredientes.isEmpty() || preco.isEmpty()) {
            return;
        }

        Pizza pizza = new Pizza(sabor, ingredientes, Float.valueOf(preco));
        Intent output = new Intent();
        output.putExtra(RESULT_KEY, pizza);
        setResult(RESULT_OK, output);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}