package br.edu.iftm.pdm.mario.myapplication.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Promocao implements Parcelable {

    private String nome;
    private String descricao;
    private float desconto;
    private float valorTotal;

    public Promocao(String nome, String descricao, float desconto, float valorTotal) {
        this.nome = nome;
        this.descricao = descricao;
        this.desconto = desconto;
        this.valorTotal = valorTotal;
    }

    protected Promocao(Parcel in) {
        nome = in.readString();
        descricao = in.readString();
        desconto = in.readFloat();
        valorTotal = in.readFloat();
    }


    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public float getDesconto() {
        return desconto;
    }

    public void setDesconto(float desconto) {
        this.desconto = desconto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    @Override
    public int describeContents() {
       return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nome);
        dest.writeString(descricao);
        dest.writeFloat(desconto);
        dest.writeFloat(valorTotal);

    }

    public static final Creator<Promocao> CREATOR = new Creator<Promocao>() {
        @Override
        public Promocao createFromParcel(Parcel in) {
            return new Promocao(in);
        }

        @Override
        public Promocao[] newArray(int size) {
            return new Promocao[size];
        }
    };
}
