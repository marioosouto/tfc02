package br.edu.iftm.pdm.mario.myapplication.ui.pizzas;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import br.edu.iftm.pdm.mario.myapplication.R;
import br.edu.iftm.pdm.mario.myapplication.model.Pizza;

public class PizzaDataAdapter extends RecyclerView.Adapter<PizzaDataViewHolder> {

    private ArrayList<Pizza> pizzas;
    private OnClickPizzaListener listener;

    public interface OnClickPizzaListener {
        void onclickPizza(Pizza pizza);
    }

    public PizzaDataAdapter(ArrayList<Pizza> pizzas) {
        this.pizzas = pizzas;
        this.listener = null;
    }

    @NonNull
    @Override
    public PizzaDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.pizza_item_view, parent, false);
        return new PizzaDataViewHolder(itemView,this);
    }

    @Override
    public void onBindViewHolder(@NonNull PizzaDataViewHolder holder, int position) {
        holder.bind(this.pizzas.get(position));
    }

    @Override
    public int getItemCount() {
        return this.pizzas.size();
    }


    public void setOnClickPizzaListener(OnClickPizzaListener listener) {
        this.listener = listener;
    }

    public OnClickPizzaListener getOnClickPizzaListener(){
        return this.listener;
    }

}