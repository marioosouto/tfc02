package br.edu.iftm.pdm.mario.myapplication.data;

import java.util.ArrayList;

import br.edu.iftm.pdm.mario.myapplication.model.Pizza;

public class DAOPizzasSingleton {

    private static DAOPizzasSingleton INSTANCE;
    private ArrayList<Pizza> pizzas;

    private DAOPizzasSingleton() {
        this.pizzas = new ArrayList<>();
    }

    public static DAOPizzasSingleton getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new DAOPizzasSingleton();
        }
        return INSTANCE;
    }

    public ArrayList<Pizza> getPizzas() {
        return this.pizzas;
    }

    public void addPizza(Pizza pizza) {
        this.pizzas.add(pizza);
    }
}
