package br.edu.iftm.pdm.mario.myapplication.ui.activitys;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import br.edu.iftm.pdm.mario.myapplication.R;
import br.edu.iftm.pdm.mario.myapplication.data.DAOPizzasSingleton;
import br.edu.iftm.pdm.mario.myapplication.model.Pizza;
import br.edu.iftm.pdm.mario.myapplication.ui.pizzas.PizzaDataAdapter;

public class PizzaMenuActivity extends AppCompatActivity {

    private RecyclerView rvPizzas;
    private PizzaDataAdapter pizzaDataAdapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_menu);

        this.rvPizzas = findViewById(R.id.rvPizzas);

        this.layoutManager = new LinearLayoutManager(this);
        this.pizzaDataAdapter = new PizzaDataAdapter(DAOPizzasSingleton.getINSTANCE().getPizzas());
        this.pizzaDataAdapter.setOnClickPizzaListener(new PizzaDataAdapter.OnClickPizzaListener() {
            @Override
            public void onclickPizza(Pizza pizza) {
                showPizzaInfo(pizza);
            }
        });

        DividerItemDecoration divider = new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);

        this.rvPizzas.setLayoutManager(this.layoutManager);
        this.rvPizzas.setHasFixedSize(true);
        this.rvPizzas.addItemDecoration(divider);
        this.rvPizzas.setAdapter(this.pizzaDataAdapter);
    }

    private void showPizzaInfo(Pizza pizza) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.pizza_info);
        View view = getLayoutInflater().inflate(R.layout.show_pizza_info_dialog, null);
        dialogBuilder.setView(view);
        TextView txtSaborPizza = view.findViewById(R.id.txtSaborDialog);
        txtSaborPizza.setText(pizza.getSabor());
        TextView txtDescricaoPizza = view.findViewById(R.id.txtDescDialog);
        txtDescricaoPizza.setText(pizza.getDescricao());
        Button btnP = view.findViewById(R.id.btnP);
        btnP.setText("R$"+String.valueOf(pizza.getValor()/4));
        Button btnM = view.findViewById(R.id.btnM);
        btnM.setText("R$"+String.valueOf(pizza.getValor()));
        Button btnG = view.findViewById(R.id.btnG);
        float res = (float) (pizza.getValor()*0.25);
        btnG.setText("R$"+String.valueOf(pizza.getValor() + res));
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        dialogBuilder.create().show();
    }


}