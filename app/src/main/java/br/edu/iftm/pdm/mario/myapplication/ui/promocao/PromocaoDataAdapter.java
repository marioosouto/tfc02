package br.edu.iftm.pdm.mario.myapplication.ui.promocao;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import br.edu.iftm.pdm.mario.myapplication.R;
import br.edu.iftm.pdm.mario.myapplication.model.Promocao;

public class PromocaoDataAdapter extends RecyclerView.Adapter<PromocaoDataViewHolder> {

    private ArrayList<Promocao> promocoes;
    private OnClickPromocaoListener listener;

    public interface OnClickPromocaoListener {
        void onclickPromocao(Promocao promocao);
    }

    public PromocaoDataAdapter(ArrayList<Promocao> promocoes) {
        this.promocoes = promocoes;
        this.listener = null;
    }

    @NonNull
    @Override
    public PromocaoDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.promocao_item_view, parent, false);
        return new PromocaoDataViewHolder(itemView,this);
    }

    @Override
    public void onBindViewHolder(@NonNull PromocaoDataViewHolder holder, int position) {
        holder.bind(this.promocoes.get(position));
    }

    @Override
    public int getItemCount() {
        return this.promocoes.size();
    }


    public void setOnClickPromocaoListener(OnClickPromocaoListener listener) {
        this.listener = listener;
    }

    public OnClickPromocaoListener getOnClickPromocaoListener(){
        return this.listener;
    }

}