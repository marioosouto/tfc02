package br.edu.iftm.pdm.mario.myapplication.data;

import java.util.ArrayList;
import br.edu.iftm.pdm.mario.myapplication.model.Promocao;

public class DAOPromocoesSingleton {

    private static DAOPromocoesSingleton INSTANCE;
    private ArrayList<Promocao> promocoes;

    private DAOPromocoesSingleton() {

        this.promocoes = new ArrayList<>();
    }

    public static DAOPromocoesSingleton getINSTANCE() {
        if (INSTANCE == null) {
            INSTANCE = new DAOPromocoesSingleton();
        }
        return INSTANCE;
    }

    public ArrayList<Promocao> getPromocoes() {

        return this.promocoes;
    }

    public void addPromocao(Promocao promocao) {
        this.promocoes.add(promocao);
    }
}
